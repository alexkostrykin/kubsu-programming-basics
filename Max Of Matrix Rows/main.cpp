#include <iostream>
#include <time.h>

using namespace std;

const int SIZE_ARRAY = 5;
int _matrix[SIZE_ARRAY][SIZE_ARRAY];
int outputArray[SIZE_ARRAY];

void MaxOfRows()
{
	for (int i = 0; i < SIZE_ARRAY; i++)
	{
		int maxOfRow = 0;
		for (int j = 0; j < SIZE_ARRAY; j++)
		{
			if (_matrix[i][j] > maxOfRow)
			{
				maxOfRow = _matrix[i][j];
			}
		}
		outputArray[i] = maxOfRow;
	}

	return;
}


int main()
{
	int _randMax = 100;
	int _randMin = 1;


	srand(time(0));

	cout << endl << "Input: " << endl;
	for (int i = 0; i < SIZE_ARRAY; i++)
	{
		for (int j = 0; j < SIZE_ARRAY; j++)
		{
			_matrix[i][j] = rand() % _randMax + _randMin;

			cout << _matrix[i][j] << " ";
		}
		cout << endl;
	}

	MaxOfRows();


	cout << endl << "Output: " << endl;

	for (int i = 0; i < SIZE_ARRAY; i++)
	{
		cout << outputArray[i] << " ";
		cout << endl;
	}
}