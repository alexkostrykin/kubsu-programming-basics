﻿// Recursive Degree.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

long double Pow(double base, int exp)
{
    if (exp < 0)
    {
        base = 1 / base;
        exp *= -1;
    }

    if (exp == 1)
    {
        return base;
    }
    if (exp != 1)
    {
        return (base * Pow(base, exp - 1));
    }
}

int main()
{
    int base = 0;
    int exp = 0;

    std::cout << "Write base: ";
    std::cin >> base;
    std::cout << "Write degree:";
    std::cin >> exp;

    std::cout << "Value = " << Pow(base, exp) << " ";

}
