#include <iostream>
using namespace std;

struct Product
{
    public: string Name;
    public: float Price;
};

struct Book : Product
{
    public: Book()  { }
    public: Book(string name, float price, string author, int year, int pages)
    {
        this -> Name = name;
        this -> Price = price;
        this -> Author = author;
        this -> Year = year;
        this -> Pages = pages;
    }

    public: string Author;
    public: int Year;
    public: int Pages;
};

const int SIZE_ARRAY = 5;
Book books[SIZE_ARRAY];

int main()
{
    setlocale(LC_ALL, "ru_RU.UTF-8");
    int totalPages = 0;

    books[0] = Book("Книга один",0,"Петров",2002,96);
    books[1] = Book("Книга два",0,"Пушкин",2002,42);
    books[2] = Book("Книга три",0,"Толстой",2002,123);
    books[3] = Book("Книга четыре",0,"Данилов",2002,23);
    books[4] = Book("Книга пять",0,"Кузнецов",2002,344);

    for (int i = 0; i < SIZE_ARRAY; ++i) {
        if(books[i].Author.substr(books[i].Author.length() - 4,4) == "ов")
        {
            cout << "Book name: " << books[i].Name << " | Book author: " << books[i].Author << endl;
            totalPages += books[i].Pages;
        }
    }

    cout << "Total pages: " << totalPages << endl;
    return 0;
}
